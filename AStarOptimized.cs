﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//
//public class AStarOptimized : MonoBehaviour {
//	
//	[SerializeField] private bool showRouteDebug = false;
//	[SerializeField] public bool showSuccessCast = false;
//	[SerializeField] private bool showFailCast = false;
//	[SerializeField] public bool debugBreak = false;
//	[SerializeField] private float offsetMultiplier = 0.0f;
//
//	private List<Node> listOpen = new List<Node>();
//	private List<Node> listClosed = new List<Node>();
//	private List<Transform> route = new List<Transform>();
//	private List<Node> listMaster;
//	private Node origin;
//	private Node destination;
//	private bool pathIsAvailable;
//	private Request activeRequest;
//	
//	bool isRunningRequests = false;
//	private NodeManager nodeManager;
//	
//	List<Request> listRequest = new List<Request>();
//	
//	class Request{
//		public Node start;
//		public Node end;
//		public Callback<List<Transform>> callback;
//		
//		public Request (Node s, Node e, Callback<List<Transform>> _callback){
//			this.start = s;
//			this.end = e;
//			callback = _callback;
//		}
//	}
//	
//	void Start(){
////		GameController.Instance.AStar = this;
//		nodeManager = GameController.Instance.NodeManager;
//		listMaster = nodeManager.listNode;
//	}
//
//	public List<Transform> FindPath(Node o, Node d, Callback<List<Transform>> callback){
//		listRequest.Add(new Request(o, d, callback));
//		if(isRunningRequests == false){
//			StartCoroutine(RunRequest());
//		}
//		return null;
//	}
//	
//	bool ProcessRequest(){
//		Clear();
//		
//		origin = activeRequest.start;//NodeManager.GetNodeWithId(activeRequest.start.locationId, listMaster, activeRequest.start.uniqueId);
//		destination = activeRequest.end;//NodeManager.GetNodeWithId(activeRequest.end.locationId, listMaster, activeRequest.end.uniqueId);
//		
//		if (origin.uniqueId == -1 || destination.uniqueId == -1){
//			Debug.LogError("WOAH!" );
//		}
//
//		/* A-Star*/
//		AStart();
//		
//		int counter = 0;
//		if(pathIsAvailable == true){
//			Node temp = destination;
//			while(temp != null){
//				if(counter++ >= 5000) { 
//					Debug.Log("Possible infinite loop on " + transform.name);
//					break;
//				}
//				
//				/* Add the spawn point to route if it exists. Otherwise, use transform. */
//				if(temp.portalSpawnPoint != null){
//					if(temp.GetParent().portalSpawnPoint != null){
//						route.Add(temp.portalSpawnPoint);
//						temp = temp.GetParent();
//					}else{
//						route.Add(temp.GetTransform());
//						temp = temp.GetParent();
//					}
//				}
//				else if(temp.portalSpawnPoint == null){
//					route.Add(temp.GetTransform());
//					temp = temp.GetParent();
//				}
//			}
//			route.Reverse();
//			route.RemoveAt(0);
//		}
//
//		
//		if(debugBreak == true && route.Count == 0){
//			Debug.Log("No Path: " +origin.name+ " to " +destination.name );
//			Debug.Break();
//		}
//
//		activeRequest.callback(new List<Transform>(route)); //TODO: OPTMIZED THIS
//		activeRequest.callback = null;
//		listRequest.Remove(activeRequest);
//		activeRequest = null;
//		return false;
//	}
//
//	void Update(){
//		if(route.Count > 0 && showRouteDebug == true){
//			if(route[0] == destination) { return; }
//			for (int i=0; i<route.Count-1; ++i){
//				Debug.DrawLine((Vector3)route[i].transform.position, (Vector3)route[i+1].transform.position, Color.green, 0.01f);
//			}
//		}
//	}
//	
//	IEnumerator RunRequest(){
//		isRunningRequests = true;
//		while(listRequest.Count > 0 ){
//			activeRequest = listRequest[0];
//
//			yield return ProcessRequest();
//		}
//		isRunningRequests = false;
//	}
//
//	void Clear(){
//
//		pathIsAvailable = false;
//		origin = null;
//		destination = null;
//		listOpen.RemoveAll(item => item != null);
//		listClosed.RemoveAll(item => item != null);
//		route.RemoveAll(item => item != null);
//
//		foreach(Node n in listMaster){
//				n.SetParent(null);
//				n.SetCost(int.MaxValue);
//				n.aiKey = "free";
//		}
//	}
//
//	List<NodeController> listInitialNeighbor = new List<NodeController>();
//	void SetInitialNeighbor(Node node){
////		node.GetTransform().GetComponent<NodeController>().listNeigbor.Clear();
//
//		List<Node> tempList = nodeManager.collectionNodes[node.mapId.ToString().ToUpper()];
//
//		for(int x=0; x<tempList.Count; x++){
//			Node neighbor = tempList[x];
//			RaycastHit hit;
//			Vector3 offset = ((neighbor.GetPos() - node.GetPos()).normalized ) * offsetMultiplier;
//			float distance =  Vector3.Distance(node.GetPos()/*+offset*/, neighbor.GetPos());
//			//		if(distance <= 0.2f || distance > 20.0f) { continue; }
//
//			if(Physics.Raycast(node.GetPos()/*+offset*/, neighbor.GetPos() - node.GetPos(),out hit,distance)){
//				if(hit.transform == neighbor.GetTransform()){
//					if(node.GetTransform().GetComponent<NodeController>().listNeigbor.Contains(tempList[x].GetTransform().GetComponent<NodeController>()) == false){
//						node.GetTransform().GetComponent<NodeController>().listNeigbor.Add(tempList[x].GetTransform().GetComponent<NodeController>());
//						listInitialNeighbor.Add(tempList[x].GetTransform().GetComponent<NodeController>());
//					}
//				}
//			}
//		}
//	}
//
//	void UnsetInitialNeighbor(Node node){
//		for(int x=0; x<listInitialNeighbor.Count; x++){
//			node.GetTransform().GetComponent<NodeController>().listNeigbor.Remove(listInitialNeighbor[x]);
//		}
//		listInitialNeighbor.Clear();
//	}
//	
//
//	#region AStar Logic
//	
//	void AStart(){
//		listOpen.Add(origin);
//		SetInitialNeighbor(origin);
//
//		int counter = 0;
//		while(listOpen.Count >0){
//			if(counter++ >= 5000) { 
//				Debug.Log("Possible infinite loop."); 
//				break;
//			}
//			
//			Node curr = listOpen[0];
//			
//			//move current node to closedList
//			curr.aiKey = "close";
//			listClosed.Add(curr);
//			listOpen.Remove(curr);
//			
//			//stop if current point is targetNode
//			if(curr == destination){
//				pathIsAvailable = true;
//				break;
//			}
//			
//			//add neighbor to openlist
//			GetNeighbors(curr);
//			
//			//sort neighbor accr to cost
//			QSort(listOpen,0,listOpen.Count-1);
//		}
//
//		UnsetInitialNeighbor(origin);
//	}
//
//	void GetNeighbors(Node node){
//		List<Node> listTemp = new List<Node>();
//		for(int x=0; x<node.GetTransform().GetComponent<NodeController>().listNeigbor.Count; x++){
//			// Skip if component is disabled.
//			if(node.GetTransform().GetComponent<NodeController>().listNeigbor[x] == null 
//			   || node.GetTransform().GetComponent<NodeController>().listNeigbor[x].enabled == false){
//				continue;
//			}
//			listTemp.Add(node.GetTransform().GetComponent<NodeController>().listNeigbor[x].node);
//		}
//
//		for(int x=0; x<listTemp.Count; x++){
//			Node neighbor = listTemp[x];
//			
//			/* Skip process if any condition below is met. */
//			if(neighbor == null) {continue;}
//			if(neighbor == node) {continue;}
//			if(neighbor.GetParent() == node) {continue;}
//			if(neighbor.isWalkable == false && neighbor != destination){continue;}
//			if(neighbor.mapId != node.mapId) {continue;}
//			if(listClosed.Contains(neighbor)) {continue;}
//			
//			RaycastHit hit;
//			Vector3 offset = ((neighbor.GetPos() - node.GetPos()).normalized ) * offsetMultiplier;
//			float distance =  Vector3.Distance(node.GetPos()/*+offset*/, neighbor.GetPos());
////			if(distance <= 0.2f || distance > 20.0f) { continue; }
//			
//			if(Physics.Raycast(node.GetPos()/*+offset*/, neighbor.GetPos() - node.GetPos(),out hit,distance)){
//				/* hit is neighbor */
//				if(hit.transform == neighbor.GetTransform()){
//					if(neighbor.aiKey != "open"/* listOpen.Find(item=> item.uniqueId == neighbor.uniqueId) == null*/){
//						if(showSuccessCast == true){ Debug.DrawLine(node.GetPos()+offset, neighbor.GetPos(), Color.green); }
//						neighbor.aiKey = "open";
//						neighbor.SetParent(node);
//						neighbor.SetCost(node.GetCost() + distance);
//						listOpen.Add(neighbor);
//					}
//					else{
//						if(neighbor.GetCost() > node.GetCost() + distance){
//							if(showSuccessCast == true){ Debug.DrawLine(node.GetPos()+offset, neighbor.GetPos(), Color.green); }
//							neighbor.SetCost(node.GetCost() + distance);
//							neighbor.SetParent(node);
//						}
//					}
//					
//					/* Check if node is portal. */
//					if(neighbor.portalDestinationId > 0){
//						Node portal = listOpen.Find(item => item.locationId == neighbor.portalDestinationId);
//						
//						/* Portal is not yet in open list or closed list.*/
//						if( portal == null  && listClosed.Find(item => item.locationId == neighbor.portalDestinationId) == null){
//							/* Get the node in master for cloning. */
//							portal = NodeManager.GetNodeWithId(neighbor.portalDestinationId, listMaster);
//							if(portal == null){ 
//								Debug.LogError(neighbor.portalDestinationId +" is not in master list."); 
//								break;
//							}
//							
//							/* Update listCopiedNode. */
////							UpdateListNode(portal);
////							portal = NodeManager.GetNodeWithId(portal.locationId, listMaster, portal.uniqueId);
////							if(portal == null){ 
////								Debug.LogError(neighbor.portalDestinationId +" is not in copied list."); 
////								break;
////							}
//							portal.SetCost(node.GetCost() + 1000);
//							portal.SetParent(neighbor);
//							portal.aiKey = "open";
//							listOpen.Add (portal);
//						}
//						/* Portal is already added in open list or closed list.*/
//						else{
//							if(portal != null){
//								float cost  = node.GetCost() + 1000;
//								if(cost < portal.GetCost()){
//									portal.SetCost(cost);
//									portal.SetParent(neighbor);
//								}
//							}
//							else{
////								if(showOtherLines == true){ Debug.Log(neighbor.portalDestinationId+ " already added in closed list. Ignore."); }
//							}
//						}
//					}
//				}
//			}
//			else{
//				if(showFailCast == true){
////					Debug.Log("Raycast didn't hit any collider");
////					Debug.DrawLine(node.GetPos()+offset, neighbor.GetPos(), Color.white, 10.1f);
//				}
//			}
//		}
//		listTemp.RemoveAll(item => item != null);
//		listTemp = null;
//	}
//
//	void QSort(List<Node> a, int left, int right ) { 
//		if( a == null || a.Count <= 1) 
//			return; 
//		int i = left; 
//		int j = right; 
//		Node n1 = listOpen[i];
//		Node n2 = listOpen[j];
//		Node nodePivot = a[( left + right ) / 2];
//		float pivot = nodePivot.GetCost(); 
//		while( i <= j ) 
//		{ 
//			while( n1.GetCost() < pivot ) {
//				i++;
//				n1 = listOpen[i];
//			}
//			while( n2.GetCost() > pivot ) {
//				j--; 
//				n2 = listOpen[j];
//			}
//			if( i <= j ) 
//			{ 
//				Node tmp = a[i]; 
//				a[i++] = a[j]; 
//				a[j--] = tmp; 
//			} 
//		} 
//		if( j > left ) 
//		{ 
//			QSort(a, left, j ); 
//		} 
//		if( i < right ) 
//		{ 
//			QSort(a, i, right ); 
//		} 
//	}
//
//	#endregion
//}
