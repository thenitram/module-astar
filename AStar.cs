﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System; 

public class AStar:MonoBehaviour {

	private List<Request> listRequest = new List<Request>();
	private List<Node> listOpen = new List<Node>();
	private List<Node> listClosed = new List<Node>();
	private List<Node> listNode;

	private Node origin;
	private Node destination;
	private Request activeRequest;

	bool isRunningRequests = false;
	
	private class Request{
		public Node start;
		public Node end;
		public Action<List<Vector3>> callback;
		
		public Request (Node start, Node end, Action<List<Vector3>> _callback){
			this.start = start;
			this.end = end;
			callback = _callback;
		}
	}

	public void StopAllRequest(){
		while(listRequest.Count > 0 ){
			Request request = listRequest[0];
			if(request.callback != null){
				request.callback = null;
			}
			listRequest.RemoveAt(0);
		}
		Clear();
	}

	public void FindPath(Node start, Node end, List<Node> listNode,Action<List<Vector3>> callback){
		if(start == null || end == null){
			Debug.Log("Missing start or end node.");
			return;
		}
		Debug.Log("Searching path...");
		this.listNode = listNode;
		listRequest.Add(new Request(start, end, callback));
		if(isRunningRequests == false){
			StartCoroutine(RunRequest());
		}
	}

	IEnumerator RunRequest(){
		isRunningRequests = true;
		while(listRequest.Count > 0 ){
			activeRequest = listRequest[0];
			yield return ProcessRequest();
		}
		isRunningRequests = false;
	}
	
	bool ProcessRequest(){
		Clear();
		List<Vector3> route = new List<Vector3>();

		origin = activeRequest.start;
		destination = activeRequest.end;

		/* A-Star*/
		AStart();

		int counter = 0;
		Node temp = destination;
		while(temp != null){
			if(counter++ >= 5000) { 
				Debug.Log("Possible infinite loop on ");
				break;
			}

			route.Add(temp.GetWorldPoint());
			temp = temp.GetParent();
		}

		route.Reverse();
//		route.RemoveAt(0);
		if(activeRequest.callback != null){
			activeRequest.callback(route);
			activeRequest.callback = null;
		}
		listRequest.Remove(activeRequest);
		activeRequest = null;
		route.Clear();
		route = null;


		return false;
	}

	void Clear(){

		origin = null;
		destination = null;
		listOpen.Clear();
		listClosed.Clear();

		if(listNode != null){
			foreach(Node n in listNode){
				n.SetParent(null);
				n.SetCost(0);
				n.SetG(0);
				n.SetG(0);
			}
		}
	}

	#region AStart Logic
	void AStart(){

		listOpen.Add(origin);

		int counter = 0;
		while(listOpen.Count >0){
			if(counter++ >= 5000) { 
				Debug.Log("Possible infinite loop."); 
				break;
			}
			
			Node curr = listOpen[0];
			
			//move current node to closedList
			listClosed.Add(curr);
			listOpen.Remove(curr);
			
			//stop if current point is targetNode
			if(curr == destination){
				break;
			}
			
			//add neighbor to openlist
			GetNeighbors(curr);
			
			//sort neighbor accr to cost
			QSort(listOpen,0,listOpen.Count-1);
		}		
	}
	
	void GetNeighbors(Node n){

		int row = n.GetRow();
		int col = n.GetCol();
		
		for(int i=-1; i<2; i++){
			for (int j=-1; j<2; j++) {

				Node neighbor = listNode.Find(a => a.GetRow() == row+j && a.GetCol() == col+i);
				if(neighbor != null && listClosed.Contains(neighbor) == false){
					if(neighbor.IsWalkable() == false) {
						continue;
					}

					/** Assign cost value if neighbor is walkable and not the current node. */
					int idX2 = row+j;
					int idY2 = col+i;
					float g = n.GetG()+10;

					
					//diagonal neighbors have different cost values
					if((i==1 && j == 1) || (i==-1 && j== 1) || (i==1 && j== -1) || (i==-1 && j== -1)){
//						g = n.GetG()+14;
						continue;
					}
					
					if(listOpen.Contains(neighbor) == false){
						listOpen.Add(neighbor);
						neighbor.SetG(neighbor.GetG() + g);
						neighbor.SetH(Mathf.Sqrt((row-idX2)*(row-idX2) + (col-idY2)*(col-idY2)));
						neighbor.SetCost(neighbor.GetG()+neighbor.GetH());
						neighbor.SetParent(n);
					}
					else{
						//compare and update values
						if(neighbor.GetG() > g){
							neighbor.SetG(g);
							neighbor.SetCost(neighbor.GetG()+neighbor.GetH());
							neighbor.SetParent(n);
						}
					}
				}
			}
		}
	}

	/// <summary>
	/// Quicksort algorithm.
	/// </summary>
	/// <param name="a">The list of nodes to be sorted.</param>
	/// <param name="left">Left.</param>
	/// <param name="right">Right.</param>
	void QSort(List<Node> a, int left, int right ) { 

		if( a == null || a.Count <= 1) 
			return; 
		int i = left; 
		int j = right; 
		Node n1 = listOpen[i];
		Node n2 = listOpen[j];
		Node nodePivot = a[( left + right ) / 2];
		float pivot = nodePivot.GetCost(); 
		while( i <= j ) { 
			while( n1.GetCost() < pivot ) {
				i++;
				n1 = listOpen[i];
			}
			while( n2.GetCost() > pivot ) {
				j--; 
				n2 = listOpen[j];
			}
			if( i <= j ) { 
				Node tmp = a[i]; 
				a[i++] = a[j]; 
				a[j--] = tmp; 
			} 
		} 
		if( j > left ) { 
			QSort(a, left, j ); 
		} 
		if( i < right ) { 
			QSort(a, i, right ); 
		} 
	}
	#endregion
}
