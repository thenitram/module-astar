﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Node{

	protected int row;
	protected int col;
	protected Vector3 worldPoint;
	protected bool isWalkable = true;

	//AI members
	protected Node parent;
	protected float cost;		//cost = g+h
	protected float h;			//heuristic
	protected float g;			//vertical or horizontal = 10, diagonal = 14
	protected string ID;		//Tile ID.

	public void Initialize(int row, int col, Vector3 worldPoint){
		this.row = row;
		this.col = col;
		this.worldPoint = worldPoint;
		this.ID = row + "-" +col;
	}

	#region Setter Getter
	public int GetRow(){
		return row;
	}

	public int GetCol(){
		return col;
	}

	public float GetCost(){
		return cost;
	}

	public float GetH(){
		return h;
	}

	public float GetG(){
		return g;
	}

	public Node GetParent(){
		return parent;
	}

	public void SetCost(float cost){
		this.cost = cost;
	}

	public void SetH(float h){
		this.h = h;
	}

	public void SetG(float g){
		this.g = g;
	}

	public void SetParent(Node parent){
		this.parent = parent;
	}

	public Vector3 GetWorldPoint(){
		return this.worldPoint;
	}

	public void SetIsWalkable(bool isWalkable){
		this.isWalkable = isWalkable;
	}

	public bool IsWalkable(){
		return isWalkable;
	}

	public string GetID(){
		return ID;
	}

	#endregion

}
